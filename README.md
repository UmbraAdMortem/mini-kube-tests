# Mini-Kube-Tests

## Description
Repository for testing kubernetes deployments.
The main branch only contains a template file structure and the actual deployments can be found in there respective branches.
The nameing convention of the branches is the following:
<OS-name>/<deployment-project-name>

The main branch also contains OS-spezific install scripts for:
* [Minikube](https://minikube.sigs.k8s.io/docs/start/)
* [KubeCtl](https://kubernetes.io/docs/tasks/tools/)
* [Podman-Desktop](https://podman-desktop.io/)
* [Helm](https://helm.sh/docs/intro/install/)

## Badges

## Dependencies 

## Installation

## Usage

## Contributing
This is a personal repository for testing kubernetes deployments, therefore I will not accept any contributions, but feel free to fork this repository and modify it for your own needs.

## Acknowledgment
[Kubernetes](https://icons8.com/icon/cvzmaEA4kC0o/kubernetes) icon by [Icons8](https://icons8.com/icon/cvzmaEA4kC0o/kubernetes)

## License
This repository is under MIT license.

